// Setup empty JS object to act as endpoint for all routes
projectData = {};

// Require Express to run server and routes
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 8000;
const countries_url = 'https://restcountries.herokuapp.com/api/v1';

var corsOptions = {
    origin: 'localhost:1000',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

// Start up an instance of app
const app = express();


/* Middleware*/
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Cors for cross origin allowance
app.use(cors());


// Initialize the main project folder
app.use(express.static('website'));


//routes

app.post('/createObject', (req, res) => {

    projectData = {
        date: req.body.date,
        temp: req.body.temp,
        content: req.body.content
    }
    res.send('done');
})

app.get('/getCeatedObject', (req, res) =>{
    res.send(projectData);
})


// Setup Server

app.listen(port, () => {
    console.log(`listening to.... ${port}`);
})

