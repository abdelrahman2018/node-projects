/* Global Variables */
var countries = document.getElementById('countries');
var generat_btn = document.getElementsByTagName('BUTTON')[0];
var zipcode = document.getElementById('zip');
var feel = document.getElementById('feelings');
var countrycode = document.getElementById('countries');
const dateDiv = document.getElementById('date');
const tempDiv = document.getElementById('temp');
const contentDiv = document.getElementById('content');
const virtual_dom = new DocumentFragment();
// const countris_url = 'https://restcountries.herokuapp.com/api/v1';
const countris_url = 'https://ajayakv-rest-countries-v1.p.rapidapi.com/rest/v1/all';
const open_weather_api_key = '8b63a3827b6026ef309422270b84262f';
const open_weather_url = 'https://api.openweathermap.org/data/2.5/weather?';


// Create a new date instance dynamically with JS
let d = new Date();
let newDate = d.getMonth()+1+'.'+ d.getDate()+'.'+ d.getFullYear();
let temp = 0;


// functions
function createCountryList (data){
    data.forEach(element => {
        let op = document.createElement('option');
        op.setAttribute("value", element.alpha2Code);
        op.textContent = element.name;
        virtual_dom.appendChild(op);
    });
    // console.log(virtual_dom.childNodes.);
    countries.appendChild(virtual_dom);
}


//requests
const getData = async (url='') => {
    const res = await fetch(url, {
        method: 'get',
        headers: {
            "x-rapidapi-host": "ajayakv-rest-countries-v1.p.rapidapi.com",
	        "x-rapidapi-key": "8b726b5c1fmsh5e2efd1937ce490p1b16ebjsn126fac1edaaf",
	        "useQueryString": true
        }
    })
    .then(async (data) => {
        let data2 = await data.json();
        console.log(data2);
        createCountryList(data2);
    })
}

const updateUI = async (url='',) => {
    const resp = await fetch(url)
    .then(async (data) =>{
        const data1 = await data.json()
        try{
            dateDiv.innerHTML = `<p>date: ${data1.date}</p>` ;
            tempDiv.innerHTML = `<p>temp: ${data1.temp}</p>`;
            contentDiv.innerHTML = `<p>content: ${data1.content}</p>`;

        }catch(error){
            console.log(error);
        }
    })
}

const postRetrivedData =  async (url='', data ={}) => {
    const resp = await fetch(url, {
        method: 'post',
        // credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
    .then((data) => {
        updateUI('/getCeatedObject');
    })
}

const getWeatherData = async (url='', zipcode, countrycode, apiKey) => {
    let fullUrl = url + `zip=${zipcode},` + `${countrycode}&appid=` + apiKey + '&units=imperial'; 
    console.log(zipcode);
    if(!zipcode)
        alert('you should enter the zipcode');
    const res = await fetch(fullUrl)
    .then(async(data) => {
        const data1 = await data.json();
            temp = data1.main.temp;
            const obj = {
                temp: temp,
                content: feel.value,
                date: newDate
            };
            // console.log(obj)
            postRetrivedData('/createObject', obj);
    })
}


//event listners
document.addEventListener('ready', getData(countris_url));

generat_btn.addEventListener('click', (e) =>{
    getWeatherData(open_weather_url, zipcode.value, countrycode.value, open_weather_api_key);
})
